import gsap from 'gsap/all';
import { Timeline } from 'gsap/gsap-core';
import config from '../../config';

export default class Animation {

    constructor() {

        this.counter = 0;

        let rocket = document.getElementsByClassName('rocket');
        this._rocketElement = rocket;

        let background = document.getElementsByClassName('background')[0];
        this._backgroundElement = background;

        this._svgPath = config.svgPath;

        this.timeline = new Timeline();

        this._rocketTween = null;

        this._backgroundElement.addEventListener('click',() => {

            if((this.counter % 2) === 0) {
                gsap.killTweensOf('.rocket');
                this._rocketTween = null;
            }
            else {
                this.timeline = new Timeline();
                this.start();
            }

            this.counter++;
        })
    }

    async start() {

        this._rocketTween = this.timeline.to(this._rocketElement, {
            duration: 5, 
            repeat: -1,
            ease: "power1.inOut",
            motionPath:{
              path: this._svgPath,
              autoRotate: true
            }
          });
    }
}